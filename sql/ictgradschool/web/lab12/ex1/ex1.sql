-- Answers to exercise 1 questions
SELECT dept
FROM unidb_courses;

SELECT semester
FROM unidb_attend;

SELECT num, dept
FROM unidb_attend;

SELECT fname, lname, country
FROM unidb_students
ORDER BY country;

SELECT fname, lname, mentor
FROM unidb_students
ORDER BY mentor;

SELECT fname, lname, office
FROM unidb_lecturers
ORDER BY office;

SELECT fname, lname, staff_no
FROM unidb_lecturers
HAVING staff_no <500;

# H.List the students whose id is greater than 1668 and less than 1824
SELECT fname, lname, id
FROM unidb_students
HAVING id >1668 AND id <1824;

# I. List the students from NZ, Australia and US
SELECT fname, lname, country
FROM unidb_students
WHERE country = 'NZ' OR country = 'AU' OR country = 'US';

# J. List the lecturers in G Block
SELECT fname, lname, office
FROM unidb_lecturers
WHERE office LIKE  'G%';

# K. List the courses not from the Computer Science Department
SELECT dept, num
FROM unidb_courses
WHERE dept != 'comp';

# L. List the students from France or Mexico
SELECT fname, lname, country
FROM unidb_students
WHERE country = 'FR' OR country = 'MX';



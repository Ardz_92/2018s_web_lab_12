-- Answers to exercise 2 questions
# A. What are the names of the students who attend COMP219?
SELECT fname, lname
FROM unidb_students AS s, unidb_attend AS a
WHERE s.id = a.id
  AND a.dept = 'comp'
  AND a.num = '219';

# B. What are the names of the student reps that are not from NZ?
SELECT fname, lname
FROM unidb_students AS s, unidb_courses AS c
WHERE s.id = c.rep_id
AND s.country != 'NZ';

# C. Where are the offices for the lecturers of 219?
SELECT fname, lname, office
FROM unidb_lecturers AS l, unidb_teach AS t
WHERE l.staff_no = t.staff_no
AND t.dept = 'comp'
AND t.num = 219;

# D. What are the names of the students taught by Te Taka?
SELECT DISTINCT fname, lname
FROM unidb_students AS s, unidb_teach AS t, unidb_attend AS a
WHERE s.id = a.id
AND t.staff_no = 707;

# E. List the students and their mentors
SELECT DISTINCT s.fname AS studentfname, s.lname AS studentlname, m.fname, m.lname
FROM unidb_students AS s, unidb_students AS m
WHERE s.mentor = m.id;

# F. Name the lecturers whose office is in G-Block as well naming the students that are
# not from NZ
SELECT fname, lname
FROM unidb_lecturers
WHERE office LIKE 'G%'
UNION
  SELECT fname, lname
  FROM unidb_students
  WHERE country != 'NZ';

# G. List the course coordinator and student rep for COMP219
SELECT l.fname, l.lname, s.fname, s.lname
FROM unidb_lecturers AS l, unidb_students AS s, unidb_courses AS c
WHERE c.coord_no = l.staff_no
AND c.rep_id = s.id
AND  c.dept = 'COMP'
AND  c.num = 219;
